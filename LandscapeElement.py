
"""
    This is the Landscape Element class
    This holds the attributes of the discrete points on the grid
    Accessing the private attributes will only be possible though
    the methods implemented.
    unit tests: test/test_landscapeElement.py
"""


class LandscapeElement:
    """
    Discrete LandscapeElement within the Landscape grid
    Attributes inlcude whether this element is land or water, and the number of Hares and Pumas on this grid
    """

    def __init__(self,landType,hareDensity,pumaDensity):
        """
        Constructor for this class. Boolean, 1, 0 expected for landtype, positive doubles expected for hare and Puma densities
        :param landType:Boolean.True is Land, False indicates water element
        :param hareDensity: Double. initial Hare population
        :param pumaDensity: Double. initial puma population
        :return:LandscapeElement
        """
        try:

            if not(isinstance(landType, bool) or landType in [1,0]):
                raise ValueError()
            if not (hareDensity >= 0 and pumaDensity >= 0):
                raise ValueError()

            self.__landType__ = landType
            self.__hareDensity__ = hareDensity
            self.__pumaDensity__ = pumaDensity
        except ValueError:
            raise ValueError("LandscapeElement initialized with invalid input parameters. Please check. "
                            "Landtype = {0}, Hare density = {1}, Puma density = {2}".format(landType, hareDensity, pumaDensity))

    def getHareDensity(self):
        """
        returns Hare density on this grid
        :return:double
        """
        return self.__hareDensity__

    def getPumaDensity(self):
        """
        returns Puma density on this grid
        :return:double
        """
        return self.__pumaDensity__

    def isLand(self):
        """
        returns Land type (land or water) on this grid.
        True = Land, False = Water
        :return:Boolean
        """
        return self.__landType__
