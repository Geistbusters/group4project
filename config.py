"""
    Parameter config file
    You can either edit these parameters at runtime using command line argument 'custom'
    or you can modify the values below and pass it to the batch job.
"""

#Hare Birth Rate
r = 0.08

#Predation Rate
a = 0.04

#Puma Birth Rate per 1 Hare Eaten
b = 0.02

#Puma Mortality Rate
m = 0.06

#Hare Diffusion Rate
k = 0.2

#Puma Diffusion Rate
l = 0.1

#timestep
dt = 0.4

#Number of timesteps
N = 500

#Output of every T timesteps
T = 100

def printParameters():
    """
    Utility function for printing all simulation parameters to screen
    Reassures the user that correct parameters are used
    """
    print '\nSettings used for Simulation\n\n'
    print 'Hare Birth Rate\t\t', r
    print 'Hare Predation Rate\t', a
    print 'Puma Birth Rate\t\t', b
    print 'Puma Mortality Rate\t', m
    print 'Hare Diffusion Rate\t', k
    print 'Puma Diffusion Rate\t', l
    print 'Timestep length\t\t', dt
    print 'Total Timesteps\t\t', N
    print 'Output After %d timesteps\n\n'%T


def userCreateConfig():
    """
    Function to allow user to input all simulation parameters from command line interactively
    Prompts user with specific message for each parameter
    Since we are resetting global variables, we will call the function globals() which returns a dict of global variables
    """
    global_variable_dict = globals()
    global_variable_dict['r'] = userInputCorrectTypeNonNegative("Input Hare Birth Rate\n",float)
    global_variable_dict['a'] = userInputCorrectTypeNonNegative("Input Hare Predation Rate\n",float)
    global_variable_dict['b'] = userInputCorrectTypeNonNegative("Input Puma Birth Rate per 1 Hare eaten\n",float)
    global_variable_dict['m'] = userInputCorrectTypeNonNegative("Input Puma Mortality Rate\n",float)
    global_variable_dict['k'] = userInputCorrectTypeNonNegative("Input Hare Diffusion Rate\n",float)
    global_variable_dict['l'] = userInputCorrectTypeNonNegative("Input Puma Diffusion Rate\n",float)
    global_variable_dict['dt'] = userInputCorrectTypeNonNegative("Input Timestep length\n",float)
    global_variable_dict['N'] = userInputCorrectTypeNonNegative("Input Number of simulation timesteps\n",int)
    global_variable_dict['T'] = userInputCorrectTypeNonNegative("After how many timesteps would you like to output?\n",int)


def userInputCorrectTypeNonNegative(userPrompt, type):
    """
    Method which reads in a value interactively from the command line
    Will loop until the user inputs the correct type of parameter and until the parameter is non negative
    :param userPrompt: Message displayed to the user about which parameter they are inputting
    :param type: the type which input must match
    """
    value = 0
    inputFlag = True
    while(inputFlag):
        try:
            value = type(raw_input(userPrompt))
            if(value >= 0.0): #break from loop if input is okay
                inputFlag = False
            else:
                print "\nERROR: Please Input Positive Parameter\n"
        except ValueError :
            print '\nERROR: Please Input Correct Type: %s\n'%type
    return value


def validateParameters():
    """
    Validates all simulation parameters by testing if they can be cast to assumed type
    Also warns if diffusion rates are too large, as they can cause a crash
    """
    try:
        negativeFlag = False

        #test float parameters
        for param in [r,a,b,m,k,l,dt]:
            if float(param) <= 0:
                print '\nERROR: Parameter %s Must Be Non-negative\n'%param
                negativeFlag = True

        #test int parameters
        for param in [N,T]:
            if int(param) <= 0:
                print '\nERROR: Parameter %s Must Be Non-negative\n'%param
                negativeFlag = True

        #check for large diffusion rates, in tests these have found to crash simulation
        for diffRate in [k,l]:
            if diffRate > 1.0:
                print '\nWARNING: Diffusion Rates are large and could cause diverging population\n'
        if(negativeFlag):
            exit();

    except ValueError:
        print '\nERROR: Parameters Inputted Do Not Conform To Type Requirements\n'
        exit()
