"""
    This is the main class for the predatorPrey Simulation program.
    The Landscape class contains a grid of LandscapeElement objects
    And configuration parameters to run the simulation.
    The grid is updated every timestep and the updated values are stored in this class.
    unit tests: test/test_landscape.py
"""

import random
from LandscapeElement import LandscapeElement


class Landscape:

    def __init__(self,initArray,config):
        """
        grid initializer, initArray contains 2 dimensional 0s and 1s for land and water respectively
        config contains simulation parameters from config file
        :param initArray: list of lists containing 1s and 0s. 1 indicates Land, 0 indicates Water grid.
        :param config: Configuration to run this landscape sim.
        :return: none
        """
        self.initGrid(initArray)
        self.initParameters(config)

    def initParameters(self,config):
        self.h_birthrate = config.r #hare birth rate
        self.h_predationrate = config.a #predation rate
        self.p_birthrate = config.b #puma birth rate per 1 hare eaten
        self.p_mortrate = config.m #puma mortality rate
        self.h_diffusionrate = config.k #hare diffusion rate
        self.p_diffusionrate = config.l #puma diffusion rate
        self.timestep = config.dt #timestep

    def initGrid(self,initArray):
        """
        Initialize the 2d (list of lists) grid landscape using the input array indicating the landscape charecteristics
        :param initArray: contains 2 dimensional 0s and 1s for water and land respectively
        :return:
        """
        self.grid = []
        # Adding the halo elems to the edges before we initialize the landscape
        haloFilledLandscape = self.addHaloToInputArray(self.getListFromNpArray(initArray))

        for i in range(0, len(haloFilledLandscape)):
            self.grid.append([])
            for j in range(0, len(haloFilledLandscape[i])):
                if (haloFilledLandscape[i][j] == 1):  # land element
                    self.addLandElement(i)
                else:
                    self.addWaterElement(i)

    def addHaloToInputArray(self, workingArray):
        """
        Add Halo points to the top, bottom, left and right edges of the input array
        Value of the halo points is set in variable haloValue.
        :param workingArray: 2d list of 0s and 1s for water and land respectively
        :return:list of lists
        """
        haloValue = 0  # halo objs will be designated as water, thus 0
        # add halo elements to left and right edges of the array
        for i in range(len(workingArray)):
            workingArray[i].insert(0, haloValue)
            workingArray[i].insert(len(workingArray[i]), haloValue)

        lenCols = len(workingArray)
        lenRows = len(workingArray[0])  # all rows should be same length, taking first row as a base
        haloRow = [haloValue] * lenRows

        # add halo rows to top and bottom of the array
        workingArray.insert(haloValue, haloRow)
        workingArray.insert(len(workingArray), haloRow)

        return workingArray

    def getListFromNpArray(self, initArray):
        """
        Takes an input numpy array and returns a list of lists which can be modified and worked on
        :param initArray: numpy array
        :return: list of lists
        """
        returnArray = []
        for a in initArray:
            returnArray.append(list(a))
        return returnArray

    def getRowSize(self):
        """
        Get number of rows in this grid.
        :return:int
        """
        if self.grid:
            return len(self.grid)

    def getColSize(self):
        """
        Get number of columns in this grid. Bases this on the first row
        Assuming grid is uniform throughout.
        :return:int
        """
        if self.grid:
            return len(self.grid[0])

    def addLandElement(self, i):
        """
        Append a Land element to the grid on row i
        Each Land element is initialized with Hare and Puma populations which follow a uniform random distribution between 0-5
        :param i: row number
        :return:none
        """
        self.grid[i].append(LandscapeElement(True, random.uniform(0, 5), random.uniform(0, 5)))

    def addWaterElement(self, i):
        """
        Append Water element to the grid on row i
        Each water element is initialized with 0 Hare and Puma population.
        :param i: row number
        :return:none
        """
        self.grid[i].append(LandscapeElement(False, 0, 0))

    def update(self):
        """
        Updates entire landscape for 1 timestep.
        Does this by taking a temporary copy (new timestep) of the grid and calculating the new population
        using values from the original grid (old timestep)
        Does not modify the existing grid (which is required for puma calc).
        It cannot use in-place update because of time-setp population mixing
        :return: none
        """
        gridCopy = self.grid  # copy whole grid to include halo region

        # runnging the update from 1 to n-1 as row 0 and row n are known halo elems
        for i in range(1, len(self.grid) - 1):
            for j in range(1, len(self.grid[i]) - 1):
                # water elements need no update
                if (self.grid[i][j].isLand()):
                    Hnew = self.newHares(i, j)
                    Pnew = self.newPumas(i, j)
                    gridCopy[i][j] = LandscapeElement(True, Hnew, Pnew)

        self.grid = gridCopy


    #lamdba operations for succinct access to hare and puma densities
    hares = lambda self, x, y: self.grid[x][y].getHareDensity()
    pumas = lambda self, x, y: self.grid[x][y].getPumaDensity()

    def newHares(self, i, j):
        """
        Calculate the new population of Hares (based on predetor-prey model) at grid location (i,j) after one timestep
        Assumption: coordinates (i,j) not in Halo region
        newHares = [
        (current Hare population)
        + (number of new Hares born)
        - (number of Hares killed by Pumas)
        + (number of Hares diffused from neighbouring locations)
                ]
        If value turns negative (Hnew <0), return 0.0. We cannot have negative amount of Hares. All hares are dead!
        more information on the equation can be found in the documentation
        :param i:x-coordinate on grid location
        :param j:y-coordinate on grid location
        :return:double
        """
        # new Hares born based on birth rate and predation rate of Hares by Puma
        Hnew = self.hares(i, j) + self.timestep * (self.h_birthrate * self.hares(i, j) - self.h_predationrate * self.hares(i, j) * self.pumas(i, j))
        # find number of neighbouring hares and land elements and add the diffusion element to return val
        N, Hneighbours = self.neighbourSum(i, j, self.hares)
        Hnew += self.timestep * self.h_diffusionrate * (Hneighbours - N * self.hares(i, j))

        return Hnew if Hnew >= 0 else 0.0

    # follows same logic as newHares but with slightly different algorithm
    def newPumas(self, i, j):
        """
        Calculate the new population of Pumas (based on predetor-prey model) at grid location (i,j) after one timestep
        Does not modify the existing grid.
        Assumption: coordinates (i,j) not in Halo region
        newPumas calculation based on predetor-prey model,
        newPumas = [
        (current Puma population)
        + (number of new Pumas born per Hare eaten)
        - (number of Pumas dieing of natural causes)
        + (number of Pumas diffused from neighbouring locations)
                ]
        If value turns negative (Pnew <0), return 0.0. We cannot have negative amount of Pumas. All Pumas are dead!
        more information on the equation can be found in the documentation
        :param i:x-coordinate on grid location
        :param j:y-coordinate on grid location
        :return:double
        """
        # birth and death terms
        Pnew = self.pumas(i, j) + self.timestep * (self.p_birthrate * self.hares(i, j) * self.pumas(i, j) - self.p_mortrate * self.pumas(i, j))
        # find number of neighbouring hares and land elements and add the diffusion element to return val
        N, Pneighbours = self.neighbourSum(i, j, self.pumas)
        Pnew += self.timestep * self.p_diffusionrate * (Pneighbours - N * self.pumas(i, j))

        return Pnew if Pnew >= 0 else 0.0


    def neighbourSum(self, i, j, density):
        """
        Finds the sum of neighbouring hares/pumas and the sum of neighbouring land elements
        Neighbours include up,down,right,left points on cartesian grid
        Loops through the neighbour grid locations, sums up the population density and number of Land points
        :param i:x-coordinate on grid location
        :param j:y-coordinate on grid location
        :param density: lamdba expression for either hare or puma density
        :return:tuple
        """
        N = 0  # number of elems which are Land
        neighbourDensity = 0  # total population of neighbours

        for z in [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]:
            # water elements have 0 hares and pumas so no check required
            neighbourDensity += density(z[0], z[1])

            if (self.grid[z[0]][z[1]].isLand()):
                N += 1

        return (N, neighbourDensity)

    def averagePopulations(self):
        """
        Finds the average Hare and Puma densities over the whole landscape (including water elements)
        Returns both values as a tuple where first index is average H density and second is the p density
        :return:tuple
        """
        Hsum = 0.0
        Psum = 0.0
        count = 0.0
        #only loop over concrete region, ignore halo
        for i in range(1,self.getRowSize()-1):
            for j in range(1,self.getColSize()-1):
                #water elements always have 0 H and 0 P populations, so don't test for is land
                Hsum += self.hares(i,j)
                Psum += self.pumas(i,j)
                count += 1.0

        return (Hsum / count, Psum / count)


