"""
    This file contains utility functions primarily used for producing
    the output for the predatorPrey Simulation.
    The output is either to standard output (print) or to PPM files.
    unit tests: test/test_outputFunctions.py
"""

from Landscape import Landscape
import os
import numpy as np
import glob


def printLandscape(my_landscape):
    """
    Util function to print the current state of the grid Landscape
    :param: my_landscape: Landscape obj
    :return:none
    """
    checkLandscapeType(my_landscape)

    print 'Left:Hares\tRight:Pumas'
    print 'average Hares: %f\taverage Pumas: %f' % my_landscape.averagePopulations()
    for i in range(1, my_landscape.getRowSize() - 1):
        # print hares
        for j in range(1, my_landscape.getColSize() - 1):
            print "%.1f" % my_landscape.grid[i][j].getHareDensity(),  # , prints on same line

        print '\t',
        # print pumas
        for j in range(1, my_landscape.getColSize() - 1):
            print "%.1f" % my_landscape.grid[i][j].getPumaDensity(),

        print  # newline


def HaresToArray(my_landscape):
    """
    Output current state of the Hares in the grid Landscape as a numpy array
    :param: my_landscape: Landscape obj
    :return:Array
    """
    checkLandscapeType(my_landscape)

    # Initialize array for population densities
    HareLandscape = np.zeros((my_landscape.getRowSize() - 2, my_landscape.getColSize() - 2))

    # Fill each array element with corresponding landscape grid's Hare density
    for i in range(1, my_landscape.getRowSize() - 1):
        for j in range(1, my_landscape.getColSize() - 1):
            HareLandscape[i - 1][j - 1] = my_landscape.grid[i][j].getHareDensity()

    return HareLandscape


def PumasToArray(my_landscape):
    """
    Output current state of the Pumas in the grid Landscape as a numpy array
    :param: my_landscape: Landscape obj
    :return:Array
    """
    checkLandscapeType(my_landscape)

    # Initialize array for population densities
    PumaLandscape = np.zeros((my_landscape.getRowSize() - 2, my_landscape.getColSize() - 2))

    # Fill each array element with corresponding landscape grid's Puma density
    for i in range(1, my_landscape.getRowSize() - 1):
        for j in range(1, my_landscape.getColSize() - 1):
            PumaLandscape[i - 1][j - 1] = my_landscape.grid[i][j].getPumaDensity()

    return PumaLandscape

def PrepareOutputDirectory(FileString):
    """
    Define save path so files are stored in user specified subdirectory.
    Creates subdirectory if it does not already exist, and clears it if it exists and is occupied.
    :return: null
    """
    SavePath = os.path.join(os.getcwd(), FileString)
    if os.path.isdir(SavePath):
        print 'Output directory already exists.'
        if os.listdir(SavePath)==[]:
            print 'Output directory already clean.'
        else:
            print 'Output directory occupied, cleaning now...'
            files = glob.glob(str(SavePath)+'/*')
            for f in files:
                os.remove(f)
    else:
        print 'Output directory does not yet exist, making it now...'
        os.mkdir(FileString)


def WriteToPpm(my_landscape, file_name, print_selection):
    """
    Utility function to write PPM file output to a file.
    Will output one of Puma or Hare densities on a single call.
    PPM File creation logic:
        PPM files follow a specific format.
        First line: P3, Second Line: Grid Dimensions X Y, Third Line: PM
        Followed by the RGB pixel values for the entire grid
        PM: maximum magnitude.
            This is set to allow other pixels to be bright enough as well.
        RGB values:
            Calculated by an operation on Difference between the Grid value and Mean summed with the Standard deviation
            The constants which operate on this are arbitrarily set to 1000 and 500 after trial and error for a best combination.
        Hares:
            The RGB pixels are stored in the yellow spectrum (R and G are filled, B remains zero)
        Pumas:
            The RGB pixels are stored in the blue green spectrum (G and B are filled, R remains zero)
        Water (empty pixels):
            All the RGB pixels are set to 0. Which is black.

    :param my_landscape: Landscape obj
    :param file_name: name of the File for the PPM output
    :param print_selection: Conditional input to allow for output of either Puma or Hare density
    :return:none
    """
    checkLandscapeType(my_landscape)

    if print_selection == 1:
        Array = HaresToArray(my_landscape)
        print "Saving Hare Density to file..."
    elif print_selection == 0:
        Array = PumasToArray(my_landscape)
        print "Saving Puma Density to file..."
    else:
        raise IOError('print_selection should be one of [0,1]. Incorrect param print_selection={0}'.format(print_selection))

    # Define variables needed for general PPM output: RGB string for pixel colors, and Array dimension ints for picture size
    RGB = ""
    (X, Y) = Array.shape

    # Define variables needed for 'pretty' output: Max, Std, and Mean to transform densities to usable pixel magnitudes,
    Max = np.amax(Array)
    Std = np.std(Array)
    Mean = np.mean(Array)
    PM = int(1000 * np.abs(Max - Mean)) + int(500 * Std)


    # Loop over landscape grid to store population density as pixel magnitudes for each square
    for i in range(0, X):
        for j in range(0, Y):

            # Store nonzero Hare density as yellow pixel in transformed magnitude.
            if Array[i][j] != 0 and print_selection == 1:
                D = int(1000 * np.abs(Array[i][j] - Mean)) + int(500 * Std)
                RGB += "    " + str(D) + " " + str(D) + " " + str(0)


            # Store nonzero Puma density as blue pixel in transformed magnitude.
            elif Array[i][j] != 0 and print_selection == 0:
                D = int(1000 * np.abs(Array[i][j] - Mean)) + int(500 * Std)
                RGB += "    " + str(0) + " " + str(D) + " " + str(D)


            # Store zero population density as dark pixel.
            else:
                RGB += "    " + str(0) + " " + str(0) + " " + str(0)

                RGB += "\n"

      
                
    # Open PPM file, write requisite PPM headers, write pixel values, and close file.
    FileName = file_name + ".ppm"
    F = open(os.path.join('./save',FileName), "wb")
    F.write("P3\n")
    F.write(str(X) + " " + str(Y) + "\n")
    F.write(str(PM) + "\n")
    F.write(RGB)
    F.close


def checkLandscapeType(my_landscape):
    """
    Checks the type of the input variable to ensure its of Landscape type. Else raise ValueError
    :param my_landscape: object to validate
    :return: none
    """
    if not isinstance(my_landscape, Landscape):
        raise ValueError("Input variable my_landscape is not of type Landscape. Please check.")
