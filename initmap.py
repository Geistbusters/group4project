"""
    This is the input module of the predatorPrey Simulation. It takes a plain text ASCII file,
    and converts its contents to a 2D numpy.array for further processing
    unit tests: test/test_input.py
"""

import numpy as np
import os.path

def load_map(map_file):
    """
    Loads map file data to a numpy array for further processing.
    Map file expected to hold true to the following format:
        First row contains the dimensions MxN.
        Row 2 to N contain the map data, which are either 1 or 0.
        1 indicates Land, 0 indicates Water.
    In case of unexpected data type (non numeric, not in [1,0]), will raise an exception and abort processing further
    :param map_file: file path to simulation map to be loaded. IOError raised if not found
    :return: numpy array
    """
    try:
        if not os.path.isfile(map_file):
            raise IOError()

        #Create an inverted view, so that array index match up with map index 
        map_data = np.loadtxt(fname = map_file, skiprows = 1, dtype = np.int32)
        #map_data = np.flipud(map_data)
        
        #Array successfully created. But have to check whether its elements are 0 or 1, as required
        for i in map_data.flatten():
            if i != 0 and i != 1:
                print('numpy.array created to represent terrain, but some elements are neither 0 nor 1. Aborting.')
                raise ValueError('numpy.array created to represent terrain, but some elements are neither 0 nor 1. Aborting.')
        
        #Confirming of success, returning array 
        print('Successfully initialized terrain map from %s' %map_file)
        return map_data        

    except ValueError:
        print('Cannot create numpy.array. There are non-numeric and/or missing elements in %s' %map_file)
    except IOError:
        print 'File not Found at ./{0}! Map file path is incorrect. Please check.'.format(map_file)
