"""
    Predator Prey Simulation
    Programming Skills - Coursework 2
    Group 4
    November, 2015
    ---------------------------------------------------------------------------------
    This file contains the entry (main) routine for the this simulation program
    It takes the input configurations and landscape map file.
    It generates the Landscape using map file.
    It prepares the output directory where the PPM output files will be stored
    It runs the simulation over N timesteps and produces the output,

    The output consists of the following:
        (1) Average Hare & Puma population every T iterations printed to std out
        (2) Hare & Puma population density PPM files saved to ./save/ folder

    For this simulation, we have two (optional) inputs
        (1) Map file which specifies the terrain. File containing 1s and 0s
        (2) Simulation parameters like Hare/Puma birth, death and diffusion rates

    This system provides the user with the option of setting the params,
    If they are not specified, it will use the default values
    This code takes in the following input parameters (optional)
    map_file = sys.argv[1]. path to the map file to be used for this simulation
    run_mode = sys,argv[2] == 'custom': allows user to input custom parameters
                                        using std input prompts
    ---------------------------------------------------------------------------------
"""

import config
import sys
from initmap import load_map
from Landscape import Landscape
from outputFunctions import *
import time

DEFAULT_MAP_FILE = 'map.txt'


def printInputHelp():
    """
    Print to standard output, the correct command line method to run this program
    """
    print('You have entered incorrect input options. Please try again')
    print('Correct syntax to run this program is as follows:')
    print('python predatorPreySimulation.py [map_file_path.*] [custom]')
    print('(Both options above are optional, and if omitted, program will use default values)')
    exit()


if(__name__ == '__main__'):

    map_file = DEFAULT_MAP_FILE

    if len(sys.argv) in [2,3]:
        map_file = sys.argv[1]
        if len(sys.argv) == 3:
            if sys.argv[2] == 'custom':
                config.userCreateConfig()
            else:
                printInputHelp()
    elif len(sys.argv) > 3:
        printInputHelp()

    #validate all parameters
    config.validateParameters()

    #print parameters to screen to reassure user
    config.printParameters()

    #grid of 0s and 1s representing landscape, is promoted to array of objects in Landscape class
    rawLandscape = load_map(map_file)

    landscape = Landscape(rawLandscape,config)

    N = config.N #number of timesteps
    T = config.T #print at every 10 updates

    PrepareOutputDirectory('save')

    #Start simulation timer
    start_time = time.time()

    #run simulation
    for i in range(1,N+1):
        landscape.update()
        if (i % T == 0):
            Hav, Pav = landscape.averagePopulations()
            print '\nTimestep %d\n'%i
            print 'Average Hare Population: %.3f'%Hav
            print 'Average Puma Population: %.3f'%Pav
            Hfile='Hares'+ str(i)
            Pfile='Pumas'+str(i)
            WriteToPpm(landscape,Hfile,1)
            WriteToPpm(landscape,Pfile,0)

    end_time = time.time()

    print("\nThe simulation took %.3f seconds.\n" % (end_time - start_time))
