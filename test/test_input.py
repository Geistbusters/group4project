import numpy as np
from unittest import TestCase
from initmap import load_map

normal_map = "test/testmap_normal.dat"
non_numeric_map = "test/testmap_non-numeric.dat"
non_zeroorone_map = "test/testmap_NonZeroOrOne.dat"


class TestInput(TestCase):
    """
    This is an implementation of unit tests for initmap.py
    Test if the numpy.array representing the terrain is created correctly.
    """    
    def test_loadmap(self):
        """
        Test load_map function. Load an expected normal input ASCI file to the function and see that it returns correctly
        np.allcose(a,b) compares two numpy float arrays to check if they are identical within machine precision threshold
        :assert: if load_map numpy array is equivalent to loading the text file again
        """
        map_data = np.loadtxt(fname = normal_map, delimiter = " ", skiprows = 1)
        self.assertTrue(np.allclose(map_data, load_map(normal_map)), msg = "Map isn't loaded correctly.")

    def test_non_numeric_map(self):
        """
        Test that function raises an exception when we pass incorrect file formats,
        This test is for non numeric data in the ASCI file.
        :assert: Exception raised when loading non-numeric data file
        """
        self.assertRaises(ValueError, load_map(non_numeric_map), msg="Incorrect file format potentially allowed to be loaded. "
                                                                    "Exception not raised for Non-numeric values. Please check" )

    def test_non_zeroorone_map(self):
        """
        Test that function raises an exception when we pass faulty input file formats.
        This test is for files containing numeric data other than 0 or 1. Function should return an exception
        :assert: function should return  value error exception
        """
        self.assertRaises(ValueError, load_map(non_zeroorone_map), msg = "Incorrect file format potentiall allowed to be loaded."
                                                                         "Exception not raised when encountering numeric values "
                                                                         "which are invalid (neither 0 or 1)")

    def test_incorrect_filepath(self):
        """
        Test that function raises an exception when we pass faulty input file path.
        We take the normal file path and append a string onto it, which should not be available in the test folder
        :assert: function should return  value error exception
        """
        incorrect_file_path = normal_map+'xyz'
        self.assertRaises(IOError, load_map(incorrect_file_path), msg = "File loading potentially has an issue. "
                                                                         "Exception not raised when passed non existent file path "
                                                                         "Please check.")

