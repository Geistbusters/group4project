from unittest import TestCase
from Landscape import Landscape
import numpy as np
import config
import outputFunctions
from outputFunctions import PrepareOutputDirectory
import os

simple_array = [[1,1,1,1]]

class TestOutputFunctions(TestCase):
    def test_HaresToArray(self):
        """
        Check if the HaresToArray method properly fills new array up to the last element
        :assert: if Hare quantity returned from HaresToArray in element (m-1,n-1)
            where mxn is the 2d array (i.e. last element) is equal to the Landscape grid value at the same point
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        i,j =len(simple_array), len(simple_array[0])
        #Apply landscape.HaresToArray()
        my_HareArray = outputFunctions.HaresToArray(my_landscape)
        self.assertAlmostEqual(my_HareArray[i-1][j-1],my_landscape.grid[i][j].getHareDensity(), msg="Hare density not being output properly. Please check.", delta=0.01)

    def test_PumasToArray(self):
        """
        Check if the PumasToArray method properly fills new array up to the last element
        :assert:if Hare quantity returned from HaresToArray in element (m-1,n-1)
            where mxn is the 2d array (i.e. last element) is equal to the Landscape grid value at the same point
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        i,j =len(simple_array), len(simple_array)
        #Apply landscape.PumasToArray()
        my_HareArray = outputFunctions.PumasToArray(my_landscape)
        self.assertEqual(my_HareArray[i-1][j-1],my_landscape.grid[i][j].getPumaDensity(), msg="Puma density not being output properly. Please check.")


    def test_PrepareOutputDirectory(self):
        """
        Check if the PrepareOutputDirectory method creates an output directory correctly
        :assert: if output file is not created or has wrong name.
        """
        PrepareOutputDirectory('OutputFile')
        self.assertTrue(os.path.isdir('OutputFile'),msg="Output directory not being created properly. Please check.")
        self.assertTrue(os.listdir('OutputFile')==[],msg="Output directory not being cleared properly. Please check.")


  
