from unittest import TestCase
from LandscapeElement import LandscapeElement


class TestLandscapeElement(TestCase):
    """
    Test class for Class LandscapeElement
    This will test the class methods and boundary conditions
    """
    def test_getHareDensity(self):
        hare_density = 10.0
        landscapeElement = LandscapeElement(True, hare_density, 0)
        self.assertAlmostEqual(landscapeElement.getHareDensity(), hare_density,msg="Incorrect Hare density returned", delta=0.01 )

    def test_getPumaDensity(self):
        puma_density = 10.0
        landscapeElement = LandscapeElement(True, 0, puma_density)
        self.assertAlmostEqual(landscapeElement.getPumaDensity(), puma_density,msg="Incorrect Puma density returned", delta=0.01 )

    def test_isLand(self):
        landType = True
        landscapeElement = LandscapeElement(landType, 0, 0)
        self.assertEqual(landscapeElement.isLand(), landType, msg ="Incorrect Land type returned. Landtype should be Land.")

    def test_land_type_water(self):
        landType = False
        landscapeElement = LandscapeElement(landType, 0, 0)
        self.assertEqual(landscapeElement.isLand(), landType, msg ="Incorrect Land type returned. Landtype should be water")

    def test_negative_hare_density(self):
        """
        Landscape element should not allow negative hare density. Init should fail
        :assert: Value error exception to be raised when trying to enter negative density details
        """
        negative_hare_density = -1
        self.assertRaises(ValueError, LandscapeElement, True, negative_hare_density, 0)

    def test_negative_puma_density(self):
        """
        Landscape element should not allow negative puma density. Init should fail and obj should not be returned.
        :assert: Value error exception to be raised when trying to enter negative density details
        """
        negative_puma_density=-1
        self.assertRaises(ValueError, LandscapeElement, True, 0, negative_puma_density)





