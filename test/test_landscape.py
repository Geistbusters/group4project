from unittest import TestCase
import numpy as np
import config
from Landscape import Landscape
from LandscapeElement import LandscapeElement

simple_array = [[1,1,1,1]]
complex_array = [[1,1,1,1],[1,1,0,0]]
water_array = [[0,0,0,0]]


class TestLandscape(TestCase):
    """
    Test Landscape class methods
    """

    def test_initParameters(self):
        """
        Asserts the validity of every config parameter setting.
        :return:
        """
        my_init_array = np.array(simple_array)
        my_r = config.r
        my_a = config.a
        my_b = config.b
        my_m = config.m
        my_k = config.k
        my_l = config.l
        my_dt = config.dt
        my_landscape = Landscape(my_init_array, config)
        self.assertEqual(my_landscape.h_birthrate, my_r, msg="Hare birthrate config not correct")
        self.assertEqual(my_landscape.h_diffusionrate, my_k, msg="Hare diffusion rate config not correct")
        self.assertEqual(my_landscape.h_predationrate, my_a, msg="Hare predation config not correct")
        self.assertEqual(my_landscape.p_birthrate, my_b, msg="Puma birthrate config not correct")
        self.assertEqual(my_landscape.p_mortrate, my_m, msg="Puma mortality rate config not correct")
        self.assertEqual(my_landscape.p_diffusionrate, my_l, msg="Puma diffusion rate config not correct")
        self.assertEqual(my_landscape.timestep, my_dt, msg="Timestep config not correct")

    def test_initGrid(self):
        """
        Asserts the array constructor behavior.
        Init the class, and check an arbitrary cell in the grid to confirm it is of type LandscapeElement
        :return:
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        self.assertTrue(isinstance(my_landscape.grid[0][0], LandscapeElement))

    def test_addHaloToInputArray(self):
        """
        taking simple_array as input, we expect the below halloed array to be returned
        :return:
        """
        my_haloed_array = [[0,0,0,0,0,0],[0,1,1,1,1,0],[0,0,0,0,0,0]]
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        my_return_array = my_landscape.addHaloToInputArray(simple_array)
        self.assertEqual(my_return_array, my_haloed_array, msg="Halo array creation has issues. Please check.")

    def test_getListFromNpArray(self):
        my_np_array = np.array(simple_array)
        my_landscape = Landscape(my_np_array, config)
        my_return_array = my_landscape.getListFromNpArray(my_np_array)
        self.assertEqual(my_return_array, simple_array,msg="Output array does not equal input NP array. Please check.")

    def test_addLandElement(self):
        """
        Construct the Landscape obj and check if Land type LandscapeElement has been created
        We check cell (1,1) as the row 0 and column 0 will be halo columns
        :return:
        """
        my_init_array = np.array(complex_array)
        my_landscape = Landscape(my_init_array, config)
        self.assertTrue(my_landscape.grid[1][1].isLand(), msg="Land element creation not successful. Please check.")

    def test_addWaterElement(self):
        """
        Construct the Landscape obj and check if Water type Landscape has been created. isLand() function should return False
        We will check cell (1,1) as the row 0 and column 0 will be halo columns
        :return:
        """
        my_init_array = np.array(water_array)
        my_landscape = Landscape(my_init_array, config)
        self.assertFalse(my_landscape.grid[1][1].isLand(), msg="Water element creation not successful. Please check.")

    def test_newHares(self):
        """
        This test case will attempt to re do the calculation performed in Landscape.newHares()
        Any change in the newHares formula should be reflected in this test case as well.
        We are going to run this on cell (1,1)
        This is the formula which is going to be used.
        newHares = [
        (current Hare population)
        + (number of new Hares born)
        - (number of Hares killed by Pumas)
        + (number of Hares diffused from neighbouring locations)
                ]
        :return:
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        i, j = 1,1
        Hnew = my_landscape.hares(i, j) + my_landscape.timestep * (my_landscape.h_birthrate * my_landscape.hares(i, j) -
                                                                   my_landscape.h_predationrate * my_landscape.hares(i, j)
                                                                   * my_landscape.pumas(i, j))
        # find number of neighbouring hares and land elements and add the diffusion element to return val
        N, Hneighbours = my_landscape.neighbourSum(i, j, my_landscape.hares)
        Hnew += my_landscape.timestep * my_landscape.h_diffusionrate * (Hneighbours - N * my_landscape.hares(i, j))
        self.assertAlmostEqual(Hnew, my_landscape.newHares(i,j), msg="New Hares test not matching test case. Please check if formula has been updated.", delta=0.01)

    def test_newPumas(self):
        """
        This test case will attempt to re do the calculation performed in Landscape.newPumas(i,j)
        Any change in the newPumas formula should be reflected in this test case as well.
        We are going to run this on cell (1,1)
        This is the formula which is going to be used.
        newPumas = [
        (current Puma population)
        + (number of new Pumas born per Hare eaten)
        - (number of Pumas dieing of natural causes)
        + (number of Pumas diffused from neighbouring locations)
                ]
        :return:
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        i, j = 1,1

        #Calculation code taken from Landscape.newPumas(i,j)
        Pnew = my_landscape.pumas(i, j) + my_landscape.timestep * (my_landscape.p_birthrate * my_landscape.hares(i, j)
                                                                   * my_landscape.pumas(i, j) - my_landscape.p_mortrate
                                                                   * my_landscape.pumas(i, j))
        N, Pneighbours = my_landscape.neighbourSum(i, j, my_landscape.pumas)
        Pnew += my_landscape.timestep * my_landscape.p_diffusionrate * (Pneighbours - N * my_landscape.pumas(i, j))

        self.assertAlmostEqual(Pnew, my_landscape.newPumas(i,j), msg="New Pumas test not matching test case. Please check if formula has been updated.", delta=0.01)


    def test_averagePopulations(self):
        """
        Checks whether average population calculator method properly computes average population.
        :assert:if average populaiton of hares or pumas does not match the funciton output
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)
        HAvg,PAvg = my_landscape.averagePopulations()
         
        Hsum = 0.0
        Psum = 0.0
        count = 0.0
        #only loop over concrete region, ignore halo
        for i in range(1,my_landscape.getRowSize()-1):
            for j in range(1,my_landscape.getColSize()-1):
                #water elements always have 0 H and 0 P populations, so don't test for is land
                Hsum += my_landscape.hares(i,j)
                Psum += my_landscape.pumas(i,j)
                count += 1.0
        
                
        self.assertEqual(HAvg,Hsum/count)
        self.assertEqual(PAvg,Psum/count)

    def test_RowAndColSizes(self):
        """
        Test the Row and Column size functions.
        :assert: if row size and column size are returned as expected
        """
        my_init_array = np.array(simple_array)
        my_landscape = Landscape(my_init_array, config)

        num_rows_returned = my_landscape.getRowSize()
        num_rows_expected = len(simple_array) + 2 #Adjust for the Halo (top and bottom rows)

        num_cols_returned = my_landscape.getColSize()
        num_cols_expected =len(simple_array[0]) + 2 #Adjust for the Halo (left and right cols)

        self.assertEqual(num_rows_returned, num_rows_expected, msg="Grid size returned has issues. "
                                                                           "Number of rows returned does not match expected value. "
                                                                           "Please check")
        self.assertEqual(num_cols_returned, num_cols_expected, msg="Grid size returned has issues. "
                                                                           "Number of Columns returned does not match expected value. "
                                                                           "Please check")
